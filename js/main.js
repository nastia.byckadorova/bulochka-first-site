const hireButton = document.querySelector("#hire-button");
const overlay = document.getElementById("overlay");
const thanksgivingCard = document.getElementsByClassName("thanksgiving")[0];
const phoneInput = document.getElementsByName("phone-number")[0];
const phoneOutput = document.querySelector(".phone-output");
let isModalVisible = false;
let phoneNumber = "";

thanksgivingCard.addEventListener("click", (event) => {
  event.stopPropagation();
});

const handleClick = (event) => {
  event.preventDefault();
  overlay.classList.add("opened");
  overlay.classList.add("fade-in");
  phoneNumber = phoneInput.value;
  phoneOutput.innerHTML = phoneNumber;
  isModalVisible = true;
};

hireButton.addEventListener("click", handleClick);

overlay.addEventListener("click", () => {
  overlay.classList.add("fade-out");
  isModalVisible = false;
});

overlay.addEventListener("animationend", () => {
  if (!isModalVisible) {
    overlay.classList.remove("opened");
    overlay.classList.remove("fade-out");
    return;
  }
  overlay.classList.remove("fade-in");
});

// #region excersise

// const styledTitle = document.querySelector("h1");
// let titleStyleAttribute = styledTitle.getAttribute("style");
// titleStyleAttribute += " opacity: 0.5;";
// styledTitle.setAttribute("style", titleStyleAttribute);

// const anchorTag = document.querySelector("a");
// console.log(anchorTag.href);

const excersiseLorems = document.querySelectorAll(".lorems p");
const highlightWordsIncludingText = (htmlElement, text, textColor) => {
  const lorems = htmlElement.innerText.split(" ");
  const newLorems = lorems.map((singleWord) =>
    singleWord.includes(text) ? `<span>${singleWord}</span>` : singleWord
  );
  const html = newLorems.join(" ");
  htmlElement.innerHTML = html;
  const spans = htmlElement.querySelectorAll("span");
  spans.forEach((span) => {
    span.style.color = textColor;
  });
};

// excersiseLorems.forEach((excersiseLorem) => {
//   if (excersiseLorem.innerText.includes("lorem success")) {
//     excersiseLorem.style.color = "green";
//   }
//   if (excersiseLorem.innerText.includes("error")) {
//     excersiseLorem.style.color = "red";
//   }
//   highlightWordsIncludingText(excersiseLorem, "sum", "pink");
// });

// #endregion
