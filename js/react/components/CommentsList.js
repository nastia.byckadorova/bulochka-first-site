const CommentsList = ({ comments, onCommentDelete }) => {
  const commentsList = comments.map((comment) => (
    <Comment
      key={comment.id}
      comment={comment}
      onDelete={() => onCommentDelete(comment.id)}
    />
  ));

  const noCommentsPlaceholder = (
    <div className="no-comments-placeholder">
      <h3 className="ui disabled header">Напишите первый комментарий!</h3>
    </div>
  );

  return (
    <div className="CommentsList">
      {commentsList.length ? (
        <ul className="ui comments">{commentsList}</ul>
      ) : (
        noCommentsPlaceholder
      )}
    </div>
  );
};
