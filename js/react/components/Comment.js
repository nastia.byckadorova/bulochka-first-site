const Comment = ({ comment, onDelete }) => {
  return (
    <li className="comment">
      <div className="avatar">
        <i className="user big circle icon"></i>
      </div>
      <div className="close-icon">
        <i className="close link icon" onClick={onDelete}></i>
      </div>
      <div className="content">
        <div className="author">{comment.author}</div>
        <div className="text">{comment.text}</div>
      </div>
    </li>
  );
};
