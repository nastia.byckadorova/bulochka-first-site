class CommentForm extends React.Component {
  state = {
    text: "",
  };

  maxCommentLength = 100;

  setText = ({ target: { value: text } }) => {
    if (text.length > this.maxCommentLength) return;

    this.setState({ text });
  };

  send = (event) => {
    const { text } = this.state;

    event.preventDefault();

    if (!text) return;

    this.props.onCommentSend({ text });
    this.setState({ text: "" });
  };

  catchEnterKey = (event) => {
    if (event.key !== "Enter") return;

    this.send(event);
  };

  render() {
    return (
      <div className="CommentForm">
        <form className="ui reply form" onSubmit={this.send} noValidate>
          <div
            className={`field${
              this.state.text.length >= this.maxCommentLength ? " error" : ""
            }`}
          >
            <label>Ваш комментарий</label>
            <textarea
              id="comment-textarea"
              rows="1"
              value={this.state.text}
              onChange={this.setText}
              onKeyPress={this.catchEnterKey}
            ></textarea>
            <span className="helper-text">{`${this.state.text.length} символов из ${this.maxCommentLength}`}</span>
          </div>
          <div className="send-button-container">
            <button className="ui blue vertical animated button" type="submit">
              <div className="hidden content">Тык!</div>
              <div className="visible content">
                <i className="paper plane icon"></i>
              </div>
            </button>
          </div>
        </form>
      </div>
    );
  }
}
