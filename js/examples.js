//#region First
// if (false) {
//   const bl = ["bul"];

//   let kek = bl;
//   kek.push("kek");

//   console.log(bl, kek);
//   console.log({ text: "bul" });

//   const trueBoolean = true;

//   const dependsOnBool = trueBoolean ? "foo" : "bar";

//   console.log(dependsOnBool);
// }
//#endregion

//#region
// const user = {
//   email: 'temoncher@yandex.ru',
//   name: {
//     first:'tema',
//     last: 'baranov'
//   },
//   phone: '89605391136',
// };
// const email = user.email;
// const name = user.name;
// const { email, name: { first, last } } = user;
// console.log(email, first);

// console.log("---------------------");

// const { log } = console;
// // const log = (string) => console.log(string);
// log("lol");
// const logger = {
//   logToConsole: log,
// };
// logger.logToConsole("Bulochka");

// const friends = ["Tema", "Olega", "Bulochka", "Andreyka"];
// console.log(friends);
// const name = "Serega";

// for (const name of friends) {
//   console.log(name);
// }

// friends.forEach((friendName) =>
//   console.log(`${name} сегодня пошел гулять с ${friendName}`)
// );
//#endregion

// console.log("---------------------");

// const newNames = {
//   bul: "Bulochka",
//   tem: "Temochka",
//   ol: "Olega",
// };
// console.log(newNames);

// console.log("---------------------");

// // const str = 'gjnjwgp';
// // const num = 2;
// // const nums = '2';
// // console.log(typeof str)
// // console.log(typeof num)
// // console.log(typeof nums)

// console.log(newNames["bul"]);

// for (const key of Object.keys(newNames)) {
//   console.log('#key', newNames["bul"])
// }

// const isBulochkaImportant = true;
// const personKey = isBulochkaImportant ? 'bul' : 'tem';
// const person = newNames[personKey];

// for (const prop in newNames) {
//   console.log(newNames[prop]);
// }
// console.log("---------------------");

// const user = { name: 'Bulochka'};
// const isUserLoggedIn = Boolean(user); // is equal to const isUserLoggedIn = !!user;

// console.log(isUserLoggedIn);

// if (isUserLoggedIn) {
//   console.log("U're logged in!");
// }
// console.log("---------------------");

// const twenFive = "25";
// const intTwenFive = Number(twenFive); // is equal to const intTwenFive = +twenFive;;

// console.log(intTwenFive + 1);

// const speak = function (name = "bulochka", time = "night") {
//   console.log(`good ${time} ${name}`);
// };
// speak(undefined, "Day");

// const bul = {
//   name: "Bulochka",
// };

// bul.sayHello = () => console.log("Hello");

// bul.sayHello();
// console.log(bul);

// const user = {
//   email: "barashek@mimimi.com",
//   username: "temoncher",
//   gender: "attack helicopter",
//   login() {
//     console.log(`${this.username} is logged in!`);
//   },
//   logout: function () {
//     console.log(`${this.email} is logged out!`);
//   },
// };

// const createUser = (name) => ({
//   email: `${name}@mimimi.com`,
//   username: name,
//   gender: "attack helicopter",
//   login() {
//     console.log(`${this.username} is logged in!`);
//   },
//   logout: function () {
//     console.log(`${this.email} is logged out!`);
//   },
// });

// const bul = createUser("bulochka");

// const newUser = user;
// bul.login();
// bul.logout();
