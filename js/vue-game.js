new Vue({
  el: "#punch-game",
  data: {
    health: 100,
    isEnded: false,
  },
  computed: {
    healthBarColor() {
      if (this.health > 70) return "success";
      
      return this.health > 30 ? "warning" : "danger";
    },
  },
  methods: {
    punch() {
      this.health -= 10;
      if (this.health <= 0) {
        this.isEnded = true;
      }
    },
    restart() {
      this.health = 100;
      this.isEnded = false;
    },
  },
});
